# Kafkacat install RHEL/CentOS

gcc-c++, git, librdkafka-devel install

```
$ yum install -y gcc-c++ git 
```

```
$ git clone https://github.com/edenhill/kafkacat
$ cd kafkacat 
$ ./configure
$ make && make install
```


```
$ kcat -V
kcat - Apache Kafka producer and consumer tool
https://github.com/edenhill/kcat
Copyright (c) 2014-2021, Magnus Edenhill
Version 1.7.1-11-gab6ce8 (librdkafka 0.11.4 builtin.features=gzip,snappy,ssl,sasl,regex,lz4,sasl_gssapi,sasl_plain,sasl_scram,plugins)

```

Local install
```
$ yum localinstall *
$ cd kafkacat 
$ ./configure
$ make && make install
```
